package com.example.gm.card_demo;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AdapterExample extends RecyclerView.Adapter<AdapterExample.ExampleViewHolder> {
    private ArrayList<CardItems> exampleList;
    private ViewGroup viewGroup;
    private Context context;

    public  AdapterExample(ArrayList<CardItems> ExampleList, Context context){
        this.exampleList = ExampleList;
        this.context = context;
    }
    public class ExampleViewHolder extends ViewHolder {
        public ImageView profile;
        public TextView name;
        public TextView film;
        public TextView film_story;
        public TextView photography;
        public TextView casting;
        public TextView makeup;
        public ImageView image;


        ExampleViewHolder(View itemView) {
            super(itemView);
            profile = itemView.findViewById(R.id.profile);
            name = itemView.findViewById(R.id.name);
            film = itemView.findViewById(R.id.film);
            film_story = itemView.findViewById(R.id.film_story);
            photography = itemView.findViewById(R.id.photography);
            casting = itemView.findViewById(R.id.casting);
            makeup = itemView.findViewById(R.id.makeup);
            image = itemView.findViewById(R.id.star_img);

            View view = itemView;
            view.setOnClickListener(new View.OnClickListener() {


                @Override public void onClick(View v) {
                    Toast.makeText(context, "cardViewClicked",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, SecondActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }

    AdapterExample(ArrayList<CardItems> exampleList) {
        this.exampleList = exampleList;
    }


    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_list, viewGroup, false);
        ExampleViewHolder vh = new ExampleViewHolder(mView);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder exampleViewHolder, int position) {
        CardItems exampleItems = exampleList.get(position);
        exampleViewHolder.profile.setImageResource(exampleItems.getProfile());
        exampleViewHolder.name.setText(exampleItems.getAustin_text());
        exampleViewHolder.film.setText(exampleItems.getFilm_text());
        exampleViewHolder.film_story.setText(exampleItems.getFilm_story());
        exampleViewHolder.photography.setText(exampleItems.getPhotography_text());
        exampleViewHolder.casting.setText(exampleItems.getCast_text());

        exampleViewHolder.makeup.setText(exampleItems.getMakeUp_text());
        exampleViewHolder.image.setImageResource(exampleItems.getStar_img());


    }

    @Override
    public int getItemCount() {
        return exampleList.size();
    }
}

