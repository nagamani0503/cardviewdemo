package com.example.gm.card_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private CardView cardView;
    private TextView sampletext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Jobs");


        //cardView.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        startActivity(new Intent(MainActivity.this, SecondActivity.class));
        //    }
        //});


        setSupportActionBar(toolbar);
        ArrayList<CardItems> exampleList = new ArrayList<>();

        exampleList.add(new CardItems(R.drawable.profile_one, "Austin", "Film",
                "Queen are a British rock band that formed in London in 1970. Their classic line-up was Freddie Mercury, Brian May, Roger Taylor, and John Deacon.",
                "photography",
                "role-casting", "makeUp/producer/camera", R.drawable.star_icon));
        exampleList.add(new CardItems(R.drawable.profile_one, "Austin", "Film",
                "Queen are a British rock band that formed in London in 1970. Their classic line-up was Freddie Mercury, Brian May, Roger Taylor, and John Deacon.",
                "photography",
                "role-casting", "makeUp/producer/camera", R.drawable.star_icon));
        exampleList.add(new CardItems(R.drawable.profile_one, "Austin", "Film",
                "Queen are a British rock band that formed in London in 1970. Their classic line-up was Freddie Mercury, Brian May, Roger Taylor, and John Deacon.", "\"photography",
                "role-casting", "makeUp/producer/camera", R.drawable.star_icon));
        exampleList.add(new CardItems(R.drawable.profile_one, "Austin", "Film",
                "Queen are a British rock band that formed in London in 1970. Their classic line-up was Freddie Mercury, Brian May, Roger Taylor, and John Deacon.",
                "photography",
                "role-casting", "makeUp/producer/camera", R.drawable.star_icon));
        exampleList.add(new CardItems(R.drawable.profile_one, "Austin", "Film",
                "Queen are a British rock band that formed in London in 1970. Their classic line-up was Freddie Mercury, Brian May, Roger Taylor, and John Deacon.",
                "photography",
                "role-casting", "makeUp/producer/camera", R.drawable.star_icon));

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new AdapterExample(exampleList, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


}
