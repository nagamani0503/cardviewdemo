package com.example.gm.card_demo;

public class CardItems {

    private int profile;
    private String austin_text;
    private String film_text;
    private String film_story;
    private String photography_text;
    private String cast_text;
    private String makeUp_text;

    private int star_img;

    public CardItems(int profile, String austin_text, String film_text, String film_story,
                     String photography_text, String cast_text, String makeUp_text, int star_img) {

                this.profile=profile;
                this.austin_text=austin_text;
                this.film_text=film_text;
                this.film_story=film_story;
                this.photography_text=photography_text;
                this.cast_text=cast_text;
                this.makeUp_text=makeUp_text;
                this.star_img=star_img;




    }

    public int getProfile() {
        return profile;
    }

    public String getAustin_text() {
        return austin_text;
    }

    public String getFilm_text() {
        return film_text;
    }

    public String getFilm_story() {
        return film_story;
    }

    public String getPhotography_text() {
        return photography_text;
    }

    public String getCast_text() {
        return cast_text;
    }

    public String getMakeUp_text() {
        return makeUp_text;
    }

    public int getStar_img() {
        return star_img;
    }
}